package adprog.teemeet.taskManagement.core.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CompleteStatusTest {
    private CompleteStatus completeStatus;

    @BeforeEach
    void setUp() {
        completeStatus = new CompleteStatus();
    }

    @Test
    void showStatus() {
        String status = completeStatus.showStatus();
        assertEquals("Complete",status);
    }
}
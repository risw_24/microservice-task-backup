package adprog.teemeet.taskManagement.repository;

import adprog.teemeet.taskManagement.model.TaskInAgenda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskInAgendaRepository extends JpaRepository<TaskInAgenda, String> {
    TaskInAgenda findByAgendaId(String agendaId);
}

package adprog.teemeet.taskManagement.core.status;

public interface Status {
    public String showStatus();
}

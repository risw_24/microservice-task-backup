package adprog.teemeet.taskManagement.service;

import adprog.teemeet.taskManagement.model.TaskLog;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TaskService {
    TaskLog makeTask(String judulTask, String memberId, String eventId, String groupId);
    void removeTask(Long id);
    void removeTaskByGroup(Long groupId);
    void removeTaskByMember(Long memberId);
    void removeTaskByEvent(Long eventId);
    List<TaskLog> getTask(Long id);
    TaskLog updateTask(Long idTask, TaskLog task);
    List<String>addAssignee(List<String> number);
    List<TaskLog> getTaskByEventId(Long eventId);
    List<TaskLog> getTaskByMemberId(Long memberId);
    List<TaskLog> getAllTasks();
    List<TaskLog> getTaskByGroupId(Long groupId);
}
